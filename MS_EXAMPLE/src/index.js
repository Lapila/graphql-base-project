import { ApolloServer } from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';

import typeDefs from './schema';
import resolvers from './resolvers';

const PORT = process.env.PORT || 4001;

const server = new ApolloServer({
  schema: buildFederatedSchema([{ typeDefs, resolvers }]),
});

server.listen(PORT).then(({ url }) => {
  console.log(`🚀 Example service ready at ${url}`);
});