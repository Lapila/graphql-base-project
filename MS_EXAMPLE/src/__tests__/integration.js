// For more details see https://www.apollographql.com/docs/apollo-server/testing/testing/

import { createTestClient } from 'apollo-server-testing';
import { ApolloServer, gql } from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation' ;

import typeDefs from '../schema';
import resolvers from '../resolvers';

// Initialize test server
var context = { user: { id: 1, username: 'michel' } };
const server = new ApolloServer({
  schema: buildFederatedSchema([{ typeDefs, resolvers }]),
  context: () => context,
});
const { query, mutate } = createTestClient(server);

// Define queries and mutations to be tested
const GET_ME = gql`
  query me {
    me {
      id
      username
    }
  }
`;

// Test queries
describe('queries', () => {
  it('fetches me', async () => {
    const res = await query({ query: GET_ME });
    expect(res).toMatchSnapshot();
  });
});
