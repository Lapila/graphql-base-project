import { gql } from 'apollo-server';

const typeDefs = gql`
  # Extend Query and Mutation from the ones declared in Gateway
  extend type Query {
    me: User
  }

  type User @key(fields: "id") {
    id: ID!
    username: String
  }
`;

export default typeDefs;