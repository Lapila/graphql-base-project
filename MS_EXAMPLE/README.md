# Microservice Example

Here is an example microservice using [Apollo Federation](https://www.apollographql.com/docs/apollo-server/federation/introduction/) with some boilerplate code.

- Copy the project
- Modify the name and description in `package.json`
- Modify the port in index.js and in Dockerfile
- Add your microservice to `docker-compose.yml` and its url in `gateway` environment