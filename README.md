# GraphQL base project microservices

Backend microservices.
This project uses [Apollo Federation](https://www.apollographql.com/docs/apollo-server/federation/introduction/) to handle microservices.

## Contents
- `gateway`: contains the gateway to all microservices
- `MS_EXAMPLE`: boilerplate code for apollo federation microservice. Copy this folder to create a new service.
- `ms_auth`: microservice with a register/login in postgresSQL

## Usage
You don't need to run `npm install` in every microservice subfolder.  
Just run `docker-compose up`.  
Don't forget to:
- add your microservice in docker-compose.yml
- copy .env.example to .env and set variables
