import 'dotenv/config';
import { ApolloServer, AuthenticationError } from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';
import jwt from 'jsonwebtoken';

import typeDefs from './schema';
import resolvers from './resolvers';

import models, { sequelize } from './models';

const PORT = process.env.PORT || 4006;

const getMe = req => {
  const token = req.headers['x-token'];

  if (token) {
    try {
      return jwt.verify(token, process.env.SECRET);
    } catch (e) {
      console.log(e);
      throw new AuthenticationError(
        'Your session expired. Sign in again.',
      );
    }
  }
  return '';
};

const server = new ApolloServer({
  schema: buildFederatedSchema([{
    typeDefs,
    resolvers,
  }]),

  formatError: error => {
    const message = error.message
      .replace('SequelizeValidationError: ', '')
      .replace('Validation error: ', '');

    return {
      ...error,
      message,
    };
  },
  context: ({ req }) => {
    return {
      models,
      me: getMe(req),
      secret: process.env.SECRET,
    };
  },
});

const eraseDatabaseOnSync = true;

sequelize.sync({ force: eraseDatabaseOnSync }).then(async () => {
  if (eraseDatabaseOnSync) {
    createDefaultUsers();
  }
  server.listen(PORT).then(({ url }) => {
    console.log(`🚀 Auth service ready at ${url}`);
  });
});

const createDefaultUsers = async () => {
  await models.User.create({
    username: 'pierre',
    email: 'pierre@hello.com',
    password: 'pierre1',
  }, {});

  await models.User.create({
    username: 'robert',
    email: 'robert@hello.com',
    password: 'robert1',
  }, {});
};
