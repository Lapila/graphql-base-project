const trip = (sequelize, DataTypes) => {
  const Trip = sequelize.define('trip', {
    text: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    longitude: {
      type: DataTypes.FLOAT,
      validate: { notEmpty: true },
    },
    latitude: {
      type: DataTypes.FLOAT,
      validate: { notEmpty: true },
    },
    type: {
      type: DataTypes.STRING,
      validate: { notEmpty: true },
    },
    street: {
      type: DataTypes.STRING,
      validate: { notEmpty: true },
    },
  });

  Trip.associate = models => {
    Trip.belongsTo(models.User);
  };

  return Trip;
};

export default trip;
