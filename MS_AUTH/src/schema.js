const { gql } = require('apollo-server');

const typeDefs = gql`
  type Query {
    users: [User!]
    user(id: ID!): User
    me: User

  }

  type Mutation {
    signUp(
      username: String!
      email: String!
      password: String!
    ): Token!
    signIn(login: String!, password: String!): Token!
  }

  type User {
    id: ID!
    username: String!
    email: String!
  }

  type Token {
    token: String!
  }
`;

module.exports = typeDefs;
