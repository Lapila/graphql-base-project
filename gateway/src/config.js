import dotenv from 'dotenv';

dotenv.config();

const environment = {
  PORT: process.env.PORT || 4000,
  MS_TRAVEL_URL: process.env.MS_TRAVEL_URL || "http://localhost:4001",
  MS_AUTH_URL: process.env.MS_AUTH_URL || "http://localhost:4002",
};

export default environment;
