import { ApolloServer } from 'apollo-server';
import { ApolloGateway } from '@apollo/gateway';

import env from './config';

const { 
  PORT,
  MS_TRAVEL_URL,
  MS_AUTH_URL,
} = env;

const gateway = new ApolloGateway({
  serviceList: [
    { name: 'travel', url: MS_TRAVEL_URL },
    { name: 'auth', url: MS_AUTH_URL },
  ]
})

const server = new ApolloServer({
  gateway,
  subscriptions: false,
});

server.listen(PORT).then(({ url }) => {
  console.log(`🚀 Gateway ready at ${url}`);
});
